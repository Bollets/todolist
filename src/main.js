import Vue from 'vue'
import App from '@/App.vue'
import '../node_modules/materialize-css/sass/materialize.scss';
// eslint-disable-next-line
import '../node_modules/materialize-css/dist/js/materialize.min.js';
import store from '@/store' 
import router from '@/router'

Vue.config.productionTip = false

// Vue.use(VueRouter)

const vue = new Vue({
  router,
  store,
  render: h => h(App)
})

vue.$mount('#app')
